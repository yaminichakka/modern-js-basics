//define UI varaibales
const form = document.querySelector('#task-form');
const tasklist = document.querySelector('.collection');
const clearbtn = document.querySelector('.clear-tasks');
const filter = document.querySelector('#filter');
const taskInput = document.querySelector('#task');

//load event listeners

loadEventListeners();

function loadEventListeners(){
    //DOM load event
    document.addEventListener('DOMContentLoaded',getTasks);
    //add tasks event
    form.addEventListener('submit',addTask);
    //remove tasks event
    tasklist.addEventListener('click',removeTask);
    //clear all tasks
    clearbtn.addEventListener('click',clearTasks);
    //filter tasks event
    filter.addEventListener('keyup',filterTasks);
}
//getTasks
function getTasks() {
    let tasks;
    if(localStorage.getItem('tasks') === null) {
        tasks = [];
    }
    else{
        tasks = JSON.parse(localStorage.getItem('tasks'));
    }

    tasks.forEach(function(task){
         //create li items
    const li = document.createElement('li');
    //Add class
    li.className = 'collection-item';
    //Create text node and append to li
    li.appendChild(document.createTextNode(task));
    //Create new link
    const link = document.createElement('a');
    //Add class
    link.className = 'delete-item secondary-content'
    //Add icon Html
    link.innerHTML = '<i class="fa fa-remove"></i>';
    //Append link to li
    li.appendChild(link);
    tasklist.appendChild(li);
    });
}

function addTask (e) {

    if(taskInput.value === '') {
        alert('Add a task');
    }

    //create li items
    const li = document.createElement('li');
    //Add class
    li.className = 'collection-item';
    //Create text node and append to li
    li.appendChild(document.createTextNode(taskInput.value));
    //Create new link
    const link = document.createElement('a');
    //Add class
    link.className = 'delete-item secondary-content'
    //Add icon Html
    link.innerHTML = '<i class="fa fa-remove"></i>';
    //Append link to li
    li.appendChild(link);
    tasklist.appendChild(li);

    //store in LS
    storeTaskInLocalStorage(taskInput.value);
    //Append li to ul
    taskInput.value = '';
    e.preventDefault();

}
//Store Tasks
function storeTaskInLocalStorage(task) {
    let tasks;
    if(localStorage.getItem('tasks') === null) {
        tasks = [];
    }
    else{
        tasks = JSON.parse(localStorage.getItem('tasks'));
    }
    tasks.push(task);

    localStorage.setItem('tasks',JSON.stringify(tasks));
}

//remove task
function removeTask(e) {
    if(e.target.parentElement.classList.contains('delete-item')) {
        if(confirm('Are you sure you want to delete?')) {
        e.target.parentElement.parentElement.remove();

        removeTaskFromLocalStorage(e.target.parentElement.parentElement);
        }
    }
}
//Remove from local Storage
function removeTaskFromLocalStorage(taskItem){
    let tasks;
    if(localStorage.getItem('tasks') === null) {
        tasks = [];
    }
    else{
        tasks = JSON.parse(localStorage.getItem('tasks'));
    }
    tasks.forEach(function(task,index){
        if(taskItem.textContent === task){
            tasks.splice(index,1);
        }
    });

    localStorage.setItem('tasks',JSON.stringify(tasks))
}


// clear tasks
function clearTasks() {
    //tasklist.innerHTML = '';

    //Faster method to clear tasks
    while(tasklist.firstChild){
        tasklist.removeChild(tasklist.firstChild);
    }
    //clear tasks from localstorage
    clearTasksFromLocalStorage();
}

//clear tasks localstorage
function clearTasksFromLocalStorage(){
    localStorage.clear();
}

//filter tasks
function filterTasks(e){
    const Text = e.target.value.toLowerCase();
    document.querySelectorAll('.collection-item').forEach
    (function(task){
        const item = task.firstChild.textContent;
        if(item.toLowerCase().indexOf(Text)!= -1){
            task.style.display = 'block';
        }else{
            task.style.display = 'none';
        }
    });
}