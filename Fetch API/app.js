document.getElementById('button1').addEventListener('click',getText);
document.getElementById('button2').addEventListener('click',getJson);
document.getElementById('button3').addEventListener('click',getApi);
//get text data
function getText(){
    fetch('text.txt').then(function(res){
       // console.log(res);
      // console.log(res.text())
      return res.text();
    })
    .then(function(data){
        console.log(data);
        document.getElementById('output').innerHTML = data;
    })
    .catch(function(err){
        console.log(err);
    });
}
//get json data
function getJson(){
    fetch('post.json')
    .then(function(res){
       return res.json();
     })
     .then(function(data){
         console.log(data);
         let output = '';
         data.forEach(function(post){
             console.log(post);

             output +=`<li>${post.id} - ${post.name}</li>`;
         });
     document.getElementById('output').innerHTML =  output ;

     })
     .catch(function(err){
         console.log(err);
     });
 }

 //get data using api
 function getApi(){
    fetch('http://jsonplaceholder.typicode.com/users')
    .then(function(res){
       return res.json();
     })
     .then(function(data){
         console.log(data);
         let output = '';
         data.forEach(function(user){
             //console.log(post);

             output +=`<li>${user.id} - ${user.name}</li>`;
         });
     document.getElementById('output').innerHTML =  output ;

     })
     .catch(function(err){
         console.log(err);
     });
 }
