
//arrow function
const sayHello = function(){
    console.log('Hello');
}
const sayHello1 = () => {
    console.log('Hey');
}
sayHello();
sayHello1();

//without async

 function myfun(){
    return "hi";
}
console.log(myfun());

//async 
async function myfun(){
   // return "hi";
    const promise = new Promise((resolve,reject)=>{setTimeout(()=>resolve('Hello'),1000);
});
const error = false;
if(!error){
    const res = await promise; //wait until promise is resolved
return res;
}else{}
    await Promise.reject(new Error('SOmething went wrong'));
}
myfun()
.then(res => console.log(res));


//another example which is more good

async function getUsers(){
    const response = await fetch('http://jsonplaceholder.typicode.com/users');
    const data = await response.json();
    return data;
}
getUsers().then(users=>console.log(users));