document.getElementById('button').addEventListener('click',loadData);

function loadData(){
    //create an XHR object
    const xhr = new XMLHttpRequest();
    //open 
     xhr.open('GET','data.txt',true);
    // console.log('READYSTATE',xhr.readyState); - readyState -1 
    // xhr.onload = function(){
    //     if(this.status === 200) {
    //         console.log(this.responseText);
    //     }
    // }
    //other method with readyStates
    xhr.onreadystatechange = function(){
   //console.log('READYSTATE',xhr.readyState); - readyState 1,2,3,4 are completed when it come sto this stage
        if(this.status === 200 && this.readyState === 4) {
           // console.log(this.responseText);
           document.getElementById('output').innerHTML = `<h1>${this.responseText}</h1>`;
        }
    }
    xhr.onerror = function () {
        console.log('Request Error....');
    }
    xhr.send();

    //readyState values
    //0 - request not intialized
    //1 - server connection established
    //2 - request achieved
    //3 - processing request
    //4 - request finished and response is ready

    //HTTP Status
    // 200 - Ok
    // 403 - Forbidden
    // 404 - Not Found
   
}
